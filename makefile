CC = gcc
CFLAGS = -lncurses -I./include/ -Wall -Wextra
SOURCES = ./src/*.c

all: rogue

rogue: $(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) -o rogue

run:
	./rogue

clean:
	rm -f rogue
