#include <rogue.h>


Entity* createPlayer(Position start_pos)
{
    Entity* newPlayer = calloc(1, sizeof(Entity));

    newPlayer->pos.y = start_pos.y;
    newPlayer->pos.x = start_pos.x;
    newPlayer->ch = '@';

    return newPlayer;
}

void handleInput(int input)
{
    Position newPos = { player->pos.y, player->pos.x };

    switch(input)
    {
        //move up
        case 'k':
            player->pos.y--;
            newPos.y--;
            break;
        //move down
        case 'j':
            player->pos.y++;
            newPos.y++;
            break;
        //move left
        case 'h':
            player->pos.x--;
            newPos.x--;
            break;
        //move right
        case 'l':
            player->pos.x++;
            newPos.x++;
            break;
        default:
            break;
    }

    movePlayer(newPos);
}

void movePlayer(Position newPos)
{
    if (map[newPos.y][newPos.x].walkable)
    {
        player->pos.y = newPos.y;
        player->pos.x = newPos.x;
    }
}
